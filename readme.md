# setup-macosx

![Badge Status](https://services-as-a-ci)

First setup tool for Mac OSX

## Description

setup-macosx is a setup tool for Mac OSX.
Perform initial setup of Mac OSX using homebrew and Ansible.

## Features

- 
- 

## Requirement

- Mac OSX (latest)
- Xcode

## Usage (before installation)

1. install Xcode

    $ sudo xcodebuild -license

2. install Homebrew

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

3. install ansible

    $ brew update
    $ brew install ansible


## Installation

    $ git clone git@bitbucket.org:yoshio_sawada/setup-macosx.git

## After installation 

    $ ansible-playbook site.yml --ask-sudo-pass

## Author

Yoshio Sawada 

